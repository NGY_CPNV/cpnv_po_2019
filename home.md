# Journée Portes Ouvertes - Nov 2019

Auteur : nicolas.glassey@cpnv.ch
Version : 16-NOV-2019

## Intention

* démontrer nos compétences aux visiteurs de la filière
* découvrir de nouveaux robots (réactivation des savoir électro et dév embarqué)

## Déroulement de la Journée
### Matin

08h45-0900      Orientation sur la matinée
                * découverte des robots -> mise en place des outils collaboration avec les 1ère années
                * 
09h00-09h30     


### Après-midi
à définir avec le groupe de l'après-midi et en fonction du retour d'expérience du matin.


### TIC

Bitbucket       Pour le wiki et le code source
Trello          Pour la publication des challenges et l'avancée des travaux

